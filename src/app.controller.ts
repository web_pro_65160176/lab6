import { Body, Controller, Get, Param, Post, Query, Req } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getDefault(): string {
    return 'Default';
  }
  @Get('hello')
  getHello(): string {
    return '<html><Body><h1>Hello</h1></Body></html>';
  }
  @Post('world')
  getWorld(): string {
    return '<html><Body><h1>world</h1></Body></html>';
  }
  @Get('test-q')
  testQuery(
    @Req() req,
    @Query('celsius') celsius: number,
    @Query('type') type: string,
  ) {
    return {
      celsius: celsius,
      type: type,
    };
  }
  @Get('test-p/:celsisus')
  testParam(@Req() req, @Param('celsisus') celsisus: number) {
    return {
      celsisus,
    };
  }
  @Post('test-body')
  testBody(@Req() req, @Body() Body, @Body('celsisus') celsisus: number) {
    return { celsisus };
  }
}
