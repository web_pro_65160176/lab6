import { Body, Controller, Get, Param, Post, Query } from '@nestjs/common';
import { TemperatureService } from './temperature.service';

@Controller('temperature')
export class TemperatureController {
  constructor(private readonly temperatureService: TemperatureService) {}
  @Get('convert')
  convert(@Query('celsisus') celsisus: string) {
    return this.temperatureService.convert(parseFloat(celsisus));
  }

  @Get('convert/:celsisus')
  convertParam(@Param('celsisus') celsisus: string) {
    return this.temperatureService.convert(parseFloat(celsisus));
  }

  @Post('convert')
  convertByPost(@Body('celsisus') celsisus: number) {
    return this.temperatureService.convert(celsisus);
  }
}
