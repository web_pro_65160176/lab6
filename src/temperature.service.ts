import { Injectable } from '@nestjs/common';

@Injectable()
export class TemperatureService {
  convert(celsisus: number) {
    return {
      celsius: celsisus,
      fahrenheit: (celsisus * 9.0) / 5 + 32,
    };
  }
}
